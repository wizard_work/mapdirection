//
//  HomeViewModel.swift
//  MapDirection
//
//  Created by Yurii Goroshenko on 13.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit
import MapKit


protocol HomeViewModelDelegate: class {
  func homeViewModel(_ viewModel: HomeViewModel, didChangeAnnotations annotations: [MKAnnotation])
  func homeViewModel(_ viewModel: HomeViewModel, didChangeDirection source: MKMapItem?, destination: MKMapItem?)
}

class HomeViewModel: NSObject {
  typealias Point = (title: String, coordinate: CLLocationCoordinate2D)
  
  // MARK: - Private properties
  private var points: [Point] = []
  private var gasStations: [Point] = []
  
  // MARK: - Public properties
  weak var delegate: HomeViewModelDelegate?
  
  
  // MARK: - Private functions
  private func getAnnotations(for points: [Point], type: PinType) -> [MKPointAnnotation] {
    var annotations: [MKPointAnnotation] = []
    
    for point in points {
      let annotation = Annotation()
      annotation.title = point.title
      annotation.coordinate = point.coordinate
      annotation.type = type
      annotations.append(annotation)
    }
    
    return annotations
  }
  
  private func getMapItems(for points: [Point]) -> (source: MKMapItem?, destination: MKMapItem?) {
    var mapItems: [MKMapItem] = []
    
    for point in points {
      let placemark = MKPlacemark(coordinate: point.coordinate, addressDictionary: nil)
      let mapItem = MKMapItem(placemark: placemark)
      mapItems.append(mapItem)
    }
    
    return (mapItems.first, mapItems.last)
  }
  
  
  // MARK: - Public functions
  func start() {
    // Setup points
    points = [
      (title: "New York City", coordinate: CLLocationCoordinate2D(latitude: 40.730610, longitude: -73.935242)),
      (title: "San Francisco, CA" , coordinate: CLLocationCoordinate2D(latitude: 37.773972, longitude: -122.431297))
    ]
    
    gasStations = [
      (title: "Cheyenne GAS", coordinate: CLLocationCoordinate2D(latitude: 41.161079, longitude: -104.805450)),
      (title: "Chicago GAS" , coordinate: CLLocationCoordinate2D(latitude: 41.881832, longitude: -87.623177)),
      (title: "Toronto GAS" , coordinate: CLLocationCoordinate2D(latitude: 43.651070, longitude: -79.347015)),
      (title: "Kearney GAS" , coordinate: CLLocationCoordinate2D(latitude: 40.69946, longitude: -99.08148)),
      (title: "Omaha GAS"   , coordinate: CLLocationCoordinate2D(latitude: 41.257160, longitude: -95.995102)),
      (title: "Salt Lake City GAS" , coordinate: CLLocationCoordinate2D(latitude: 40.758701, longitude: -111.876183)),
    ]
//    delegate?.homeViewModel(self, didChangeAnnotations: getAnnotations(for: gasStations, type: .gasStation)) // show all gas stations
    
    let mapItems = getMapItems(for: points)
    
    delegate?.homeViewModel(self, didChangeAnnotations: getAnnotations(for: points, type: .point))
    delegate?.homeViewModel(self, didChangeDirection: mapItems.source, destination: mapItems.destination)
  }
  
  func showPlacesNearBy(_ route: MKRoute) {
    for step in route.steps {
      DispatchQueue.global(qos: .background).async { [weak self] in // I think it`s necessary for a large number of gas stations
        guard let welf = self else { return }
        let circleOverlay = MKCircle(mapRect: step.polyline.boundingMapRect)
        // let circleOverlay = MKCircle(center: step.polyline.coordinate, radius: 100000) // This may be a fixed radius option, if needed.
        
        for gasStation in welf.gasStations {
          let location = CLLocation(latitude: gasStation.coordinate.latitude, longitude: gasStation.coordinate.longitude)
          let center = CLLocation(latitude: circleOverlay.coordinate.latitude, longitude: circleOverlay.coordinate.longitude)
          if location.distance(from: center) < circleOverlay.radius {
           
            DispatchQueue.main.async { [weak self] in
              guard let welf = self else { return }
              welf.delegate?.homeViewModel(welf, didChangeAnnotations: welf.getAnnotations(for: [gasStation], type: .gasStation))
            }
            
          }
        }
      }
    }
  }
}
