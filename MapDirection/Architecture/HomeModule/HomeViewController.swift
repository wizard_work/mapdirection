//
//  HomeViewController.swift
//  MapDirection
//
//  Created by Yurii Goroshenko on 13.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit
import MapKit


class HomeViewController: UIViewController {
  
  // MARK: - Private properties
  @IBOutlet private weak var mapView: MKMapView!
  let viewModel: HomeViewModel = HomeViewModel()
  
  // MARK: - Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    mapView.delegate = self
    viewModel.delegate = self
    viewModel.start()
  }
  
}

// MARK: - HomeViewModelDelegate
extension HomeViewController: HomeViewModelDelegate {
  func homeViewModel(_ viewModel: HomeViewModel, didChangeDirection source: MKMapItem?, destination: MKMapItem?) {
    let directionRequest = MKDirections.Request()
    directionRequest.source = source
    directionRequest.destination = destination
    directionRequest.transportType = .automobile
    
    let directions = MKDirections(request: directionRequest)
    directions.calculate { [weak self] (response, error) -> Void in
      guard let welf = self else { return }
      guard let response = response else {
        if let error = error {
          print("Error: \(error)")
        }
        return
      }
      
      let route = response.routes[0]
      let region = MKCoordinateRegion(route.polyline.boundingMapRect)
      welf.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
      welf.mapView.setRegion(region, animated: true)
      welf.viewModel.showPlacesNearBy(route)
    }
  }
  
  func homeViewModel(_ viewModel: HomeViewModel, didChangeAnnotations annotations: [MKAnnotation]) {
    mapView.addAnnotations(annotations)
  }
}

// MARK: - MKMapViewDelegate
extension HomeViewController: MKMapViewDelegate {
  func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
    let renderer = MKPolylineRenderer(overlay: overlay)
    renderer.strokeColor = UIColor.red
    renderer.lineWidth = 4.0
    
    return renderer
  }
  
  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    let type = (annotation as? Annotation)?.type ?? PinType.point
    let annotationView = MKPinAnnotationView()
    annotationView.pinTintColor = type == PinType.point ? .red : .blue
    return annotationView
  }
}
