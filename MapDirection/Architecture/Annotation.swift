//
//  Annotation.swift
//  MapDirection
//
//  Created by Yurii Goroshenko on 13.04.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit
import MapKit

enum PinType: String {
  case point
  case gasStation
}

class Annotation: MKPointAnnotation {
  var type: PinType?
  
}

